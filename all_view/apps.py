from django.apps import AppConfig


class AllViewConfig(AppConfig):
    name = 'all_view'
