from django.urls import path
from .views import all_list, ListAllGet

app_name = 'all_view'

urlpatterns = [
    path('', all_list, name='all_view'),
    path('get_data/', ListAllGet.as_view(), name='rest_list'),
    path('get_data/<int:year>/', ListAllGet.as_view(), name='rest_list_year'),
]