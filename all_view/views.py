from django.shortcuts import render
from django.http import JsonResponse
from django.views import View
from django.conf import settings
from income.models import Income
from load_amount.models import Money
from chain.models import ChainModel
from loan.models import loan
from returns.models import Returns
from reversal.models import Reversal
from django.contrib.auth.decorators import login_required
# Create your views here.


def all_list(request):
    income_rows = Income.objects.all()
    income_form = income_rows
    load_rows = Money.objects.all()
    load_form = load_rows
    chain_rows = ChainModel.objects.all()
    chain_form = chain_rows
    loan_rows = loan.objects.all()
    loan_form = loan_rows
    returns_rows = Returns.objects.all()
    returns_form = returns_rows
    return render(request, 'app/all_view.html',
                  {'income_form': income_form, 'load_form': load_form, 'chain_form': chain_form,
                   'loan_form': loan_form, 'returns_form':returns_form})

def all_list_rest():
    income_rows = Income.objects.all()[:10]
    income_form = income_rows
    load_rows = Money.objects.all()[:10]
    load_form = load_rows
    chain_rows = ChainModel.objects.all()[:10]
    chain_form = chain_rows
    loan_rows = loan.objects.all()[:10]
    loan_form = loan_rows
    returns_rows = Returns.objects.all()[:10]
    returns_form = returns_rows
    return income_form, load_form, chain_form, loan_form, returns_form

class ListAllGet(View):

    def get(self, request):
        months = []
        method = request.GET.get("method", None)
        filter = request.GET.get("filter", None)
        month = request.GET.get("month", None)
        year = request.GET.get("year", None)
        view = request.GET.get("view", None)
        if month is not None:
            months = month.split(',')
        print(method, filter, months)
        income_form_list = []
        load_form_list = []
        chain_form_list = []
        loans_form_list = []
        returns_form_list = []
        reversal_rows_list = []

        if method == "income":

            if filter is None:
                income_rows = Income.objects.all()
                for each_income in income_rows:
                    income_form_list.append({'id':each_income.id, 'credited_by':each_income.credited_by, 'income_got':each_income.income_got, "income_source":each_income.income_source,
                                             'description':each_income.description, 'income_date':each_income.income_date, 'credited_to': each_income.credited_to})
            else:
                for each_category in settings.GET_MONEY:
                    income_rows = []

                    # Each Month
                    if (year is not None) and (month is not None):
                        for each_month in months:
                            income_sum = 0
                            income_row_each_month = Income.objects.filter(income_source=each_category[0],
                                                                           income_date__year=year,
                                                                           income_date__month=each_month)
                            for each_income in income_row_each_month:
                                income_sum += int(each_income.income_got)

                            income_rows.append(income_sum)

                    # Each Year
                    elif (year is not None):
                        income_rows_each_year = Income.objects.filter(income_source=each_category[0],
                                                                       income_date__year=year)
                        for each_income in income_rows_each_year:
                            income_sum += each_income.income_got
                        income_rows.append(income_sum)

                    # Total income
                    else:
                        income_rows_total = Income.objects.filter(expense_category=each_category[0])
                        for each_income in income_rows_total:
                            income_sum += each_income.income_got
                        income_rows.append(income_sum)

                    temp_dict = {}
                    if view == "pie":
                        temp_dict = {'name': each_category[0], 'items': 0}
                        for each_row in income_rows:
                            temp_dict['items'] += int(each_row)
                        temp_dict['items'] = [temp_dict['items']]
                    elif view == "bar":
                        temp_dict = {'name': each_category[0], 'items': []}
                        for each_row in income_rows:
                            temp_dict['items'].append(each_row)

                    income_form_list.append(temp_dict)

        if method == "expense":

            if filter is None:
                expenses_rows = Money.objects.all()
                for each_row in expenses_rows:
                    load_form_list.append({'id':each_row.id, 'money_spent':each_row.money_spent,
                                           'money_source':each_row.money_source, 'expense_category':each_row.expense_category,
                                           'description':each_row.description, 'purchase_date':each_row.purchase_date})
            else:
                for each_category in settings.CATEGORY:
                    expenses_rows = []

                    # Each Month
                    if (year is not None) and (month is not None):
                        for each_month in months:
                            expense_sum = 0
                            expenses_row_each_month = Money.objects.filter(expense_category=each_category[0],
                                                                 purchase_date__year=year, purchase_date__month=each_month)

                            for each_spent in expenses_row_each_month:
                                expense_sum += int(each_spent.money_spent)

                            expenses_rows.append(expense_sum)

                    # Each Year
                    elif(year is not None):
                        expenses_rows_each_year = Money.objects.filter(expense_category=each_category[0],
                                                             purchase_date__year=year)
                        for each_spent in expenses_rows_each_year:
                            expense_sum += each_spent.money_spent
                        expenses_rows.append(expense_sum)

                    # Total expense
                    else:
                        expenses_rows_total = Money.objects.filter(expense_category=each_category[0])
                        for each_spent in expenses_rows_total:
                            expense_sum += each_spent.money_spent
                        expenses_rows.append(expense_sum)

                    temp_dict = {}
                    if view == "pie":
                        temp_dict = {'name': each_category[0], 'items': 0}
                        for each_row in expenses_rows:
                                temp_dict['items'] += int(each_row)
                        temp_dict['items'] = [temp_dict['items']]
                    elif view == "bar":
                        temp_dict = {'name':each_category[0], 'items':[]}
                        for each_row in expenses_rows:
                                temp_dict['items'].append(each_row)

                    load_form_list.append(temp_dict)

        chain_rows = ChainModel.objects.all()
        for each_row in chain_rows:
            chain_form_list.append({'id':each_row.id, 'income_chain': each_row.income_chain.id, 'money_spent':each_row.money_spent,
                                    'money_destination': each_row.money_destination, 'description': each_row.description, 'purchase_date': each_row.purchase_date})
        loan_rows = loan.objects.all()
        for each_row in loan_rows:
            loans_form_list.append({'id':each_row.id, 'credited_by': each_row.credited_by, 'credited_to': each_row.credited_to, 'loan_got':each_row.loan_got,
                                    'loan_tenure': each_row.loan_tenure, 'description': each_row.description, 'loan_date': each_row.loan_date})
        returns_rows = Returns.objects.all()
        for each_row in returns_rows:
            returns_form_list.append({'id':each_row.id, 'return_link': each_row.return_link.id, 'amount': each_row.amount, 'description': each_row.description,
                                      'purchase_date': each_row.purchase_date, 'credited_to': each_row.credited_to, 'income_id': each_row.income_id})
        reversal_rows = Reversal.objects.all()
        for each_row in reversal_rows:
            reversal_rows_list.append({'id':each_row.id, 'chain_link': each_row.chain_link_id , 'loan_link': each_row.loan_link_id,
                                       'income_link': each_row.income_link_id, 'returns_link': each_row.returns_link_id, 'reversal_amount': each_row.reversal_amount,
                                       'source_link': each_row.source_link, 'description': each_row.description, 'purchase_date': each_row.purchase_date, 'reversal_loan': each_row.reversal_loan_id})
        if method == "income":
            return JsonResponse({'data':income_form_list})
        if method == "expense":
            return JsonResponse({'data':load_form_list})
        if method == "chain":
            return JsonResponse({"data": chain_form_list})
        if method == "loans":
            return JsonResponse({"data": loans_form_list})
        if method == "returns":
            return JsonResponse({"data": returns_form_list})
        if method == "reversal":
            return JsonResponse({"data": reversal_rows_list})

        return JsonResponse({'data':income_form_list})
