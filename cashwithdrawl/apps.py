from django.apps import AppConfig


class CashwithdrawlConfig(AppConfig):
    name = 'cashwithdrawl'
