from django import forms
from .models import CashWithdrawl


class CashWithdrawlForm(forms.ModelForm):
    class Meta:
        model = CashWithdrawl
        fields = ['money_spent', 'money_source', 'description', 'purchase_date', 'expense_link', 'income_link', 'chain_link', 'loan_link', 'user']

