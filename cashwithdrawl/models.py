from django.db import models
from django.conf import settings
# Create your models here.


class CashWithdrawl(models.Model):
    user = models.ForeignKey('user.UserProfile', on_delete=models.CASCADE, related_name='user_withdrawl',
                                  default=None, null=True, blank=True)
    loan_link = models.ForeignKey('loan.loan', on_delete=models.CASCADE, related_name='withdrawl_loan_related',
                                  default=None, null=True, blank=True)
    chain_link = models.ForeignKey('chain.ChainModel', on_delete=models.CASCADE, related_name='withdrawl_chain_income',
                                   default=None, null=True, blank=True)
    income_link = models.ForeignKey('income.Income', on_delete=models.CASCADE, related_name='withdrawl_income', default=None,
                                    null=True, blank=True)
    returns_link = models.ForeignKey('returns.Returns', on_delete=models.CASCADE, related_name='withdrawl_reverse_returns_link',
                                     default=None,
                                     null=True, blank=True)
    expense_link = models.CharField(max_length=10, choices=settings.SOURCE, default=settings.SOURCE[0][0])
    money_spent = models.PositiveIntegerField()
    money_source = models.CharField(max_length=10, choices=settings.BANKS, default=settings.BANKS[1][0])
    description = models.CharField(max_length=200, default=None, null=True)
    purchase_date = models.DateField()

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)