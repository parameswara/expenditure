from django.urls import path
from .views import GetCashWithdrawl, GetBalance

app_name = 'cashwithdrawl'

urlpatterns = [
    path('dropdown/', GetCashWithdrawl, name='get_exp_withdrawl'),
    path('<int:id>/', GetBalance, name='get_exp_balance')

]