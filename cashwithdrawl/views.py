from django.shortcuts import render
from .models import CashWithdrawl
from load_amount.models import Money
from django.http import HttpResponseRedirect, HttpResponse

from django.http import JsonResponse
# Create your views here.

def GetCashWithdrawl(request):
    # request.user.id = 1
    all_data = []
    exp_withdrawl = CashWithdrawl.objects.filter(user = request.user.id)
    for each_withdrawl in exp_withdrawl:
        temp = {'id': str(each_withdrawl.id), 'desp':str(each_withdrawl.description)}
        all_data.append(temp)
    return JsonResponse({'data': all_data})


def GetBalance(request, id):
    balance = 0
    exp_withdrawl = CashWithdrawl.objects.get(id=id)
    initial_balance = exp_withdrawl.money_spent
    expense = Money.objects.filter(withdraw_link=exp_withdrawl)
    for each_expense_money in expense:
        balance += int(each_expense_money.money_spent)

    return JsonResponse({'data': int(initial_balance - balance)})


