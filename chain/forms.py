from django import forms
from .models import ChainModel

class ChainForm(forms.ModelForm):
    class Meta:
        model = ChainModel
        fields = ['income_chain', 'money_spent', 'money_destination', 'description', 'purchase_date']