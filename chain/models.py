from django.db import models
from income.models import Income
# Create your models here.
from django.conf import settings


class ChainModel(models.Model):
    income_chain = models.ForeignKey('income.Income', on_delete=models.CASCADE, related_name='chain', default=None,
                                    null=True)
    money_spent = models.PositiveIntegerField()
    money_destination = models.CharField(max_length=10, choices=settings.BANKS, default=settings.BANKS[1][0])
    description = models.CharField(max_length=200, default=None, null=True)
    purchase_date = models.DateField()

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)