from django.urls import path, include
from . import views

app_name = 'chain'

urlpatterns = [
    path('', views.chainview, name='get_chain_income'),
    path('chain/<int:id>/', views.update, name='update_chain_income'),
    path('dropdown/', views.chain_dropdown, name='chain_dropdown')
]