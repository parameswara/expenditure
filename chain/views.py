from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import ChainForm
from .models import ChainModel
from all_view.views import all_list_rest
from django.contrib.auth.decorators import login_required
from user.models import UserProfile
from load_amount.models import Money
from chain.models import ChainModel
from cashwithdrawl.models import CashWithdrawl
from returns.models import Returns
from django.http import JsonResponse
# Create your views here.

@login_required
def chainview(request):
    # request.user.id = 1
    user = UserProfile.objects.get(user_id=request.user.id)
    if request.method == "POST":
        form = ChainForm(request.POST)
        if form.is_valid():
            user.commits += 1
            user.save()
            form.save()
            return JsonResponse({"status": 200, "message":"successfully uploaded"})

    else:
        form = ChainForm()
    income_form, load_form, chain_form, loan_form, returns_form = all_list_rest()
    return render(request, 'app/chain_income.html',
                  {'form': form, 'income_form': income_form, 'load_form': load_form, 'chain_form': chain_form,
                   'loan_form': loan_form, 'returns_form':returns_form})


def update(request, id):
    if request.method == "POST":
        data = ChainModel.objects.get(id=id)
        data.income_chain = request.POST['income_chain']
        data.money_spent = request.POST['money_spent']
        data.money_destination = request.POST['money_destination']
        data.description = request.POST['description']
        data.purchase_date = request.POST['purchase_date']
        data.save()
        return JsonResponse({"status": 200, "message":"successfully uploaded"})
    elif request.method == "GET":
        balance = 0
        data = ChainModel.objects.get(id=id)
        initial_balance = data.money_spent
        expense = Money.objects.filter(chain_link=id)
        for each_expense_money in expense:
            if Returns.objects.filter(return_link=each_expense_money.id).exists():
                ret = Returns.objects.filter(return_link=each_expense_money.id)
                for each_retrun in ret:
                    if each_retrun.income_id is not None:
                        balance += int(each_expense_money.money_spent)
            else:
                balance += int(each_expense_money.money_spent)
        expense = CashWithdrawl.objects.filter(chain_link=id)
        for each_expense_money in expense:
            balance += int(each_expense_money.money_spent)

        return JsonResponse({'data': int(initial_balance - balance)})

def chain_dropdown(request):
    all_data = []
    all_data_object = ChainModel.objects.all()
    for each_withdrawl in all_data_object:
        temp = {'id': str(each_withdrawl.id), 'desp':str(each_withdrawl.id) + ' ' + str(each_withdrawl.description)}
        all_data.append(temp)
    return JsonResponse({'data': all_data})
