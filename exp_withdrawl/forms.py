from django import forms
from .models import ExpWithDrawl

class ExpWithDrawlForm(forms.ModelForm):
    class Meta:
        model = ExpWithDrawl
        fields = ['withdrawl_source', 'money_spent', 'expense_category', 'description', 'purchase_date']
