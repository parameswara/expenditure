from django.db import models
from load_amount.models import Money
# Create your models here.
from django.conf import settings


class ExpWithDrawl(models.Model):
    withdrawl_source = models.ForeignKey('load_amount.Money', on_delete=models.CASCADE, related_name='expense_withdrawal', default=None,
                                    null=True)
    money_spent = models.PositiveIntegerField()
    expense_category = models.CharField(max_length=20, choices=settings.CATEGORY, default=settings.CATEGORY[3][0])
    description = models.CharField(max_length=200, default=None, null=True)
    purchase_date = models.DateField()

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)
