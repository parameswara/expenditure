from django.urls import path
from . import views

app_name = 'exp_withdrawl'

urlpatterns = [
    path('', views.form_data, name='exp_withdrawl_get_data')
]