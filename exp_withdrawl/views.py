from django.shortcuts import render
from django.http import JsonResponse
from .forms import ExpWithDrawlForm
from .models import ExpWithDrawl
# Create your views here.


def form_data(request):
    if request.method == "POST":
        form = ExpWithDrawlForm(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({"status": 200, "message":"successfully uploaded"})