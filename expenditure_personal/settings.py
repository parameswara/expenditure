"""
Django settings for expenditure_personal project.

Generated by 'django-admin startproject' using Django 2.1.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'whkehb00l(qt0if)f_*h=%os*m*iil(rccwgw7gesf)$8n1f*+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'income',
    'load_amount',
    'chain',
    'all_view',
    'loan',
    'returns',
    'reversal',
    'user',
    'exp_withdrawl',
    'cashwithdrawl',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'expenditure_personal.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'expenditure_personal.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR),
)
CORS_ORIGIN_WHITELIST = (
    'google.com',
    'hostname.example.com',
    'localhost:8000',
    '127.0.0.1:5000',
    '13.59.196.188'
)

BANKS = (
        ('icici_cc', 'ICICI CC'),
        ('sbi_cc', 'SBI CC'),
        ('hdfc_cc', 'HDFC CC'),
        ('icici_dc', 'ICICI DC'),
        ('hdfc_dc', 'HDFC DC'),
        ('sbi_dc', 'SBI DC'),
        ('kvb', 'kvb'),
        ('h_c', 'Hand Cash'),
    )
GET_MONEY = (
        ('salary', 'Salary'),
        ('project', 'project'),
        ('others', 'others'),
        ('h_l', 'Hand Loan'),
        ('gift', 'gift'),
        ('loan', 'loan'),
        ('cash_dep', 'Cash Deposited'),
        ('credit_inst', 'Credit Interest'),
    )
CATEGORY = (
        ('food', 'food'),
        ('cloth', 'cloth'),
        ('petrol', 'petrol'),
        ('groceries', 'groceries'),
        ('veggies', 'veggies'),
        ('petrol', 'petrol'),
        ('entertainment', 'entertainment'),
        ('medical', 'medical'),
        ('fashion', 'fashion'),
        ('interest', 'interest'),
        ('bank_charges', 'Bank Charges'),
        ('phone_charges', 'Phone Charges'),
        ('home_internet', 'Home Internet'),
        ('travel', 'travel'),
        ('electric', 'Electrical'),
        ('cash_with', 'Cash Withdrawl'),
        ('loan_given', 'Hand Loan'),
        ('education', 'Education'),
        ('others', 'others')
    )
SOURCE = (
    ('income', 'Income'),
    ('chain', 'Chain'),
    ('loan', 'Loan'),
    (' ', '------')
)
SELECT_LINK = (
        ('chain', 'Chain'),
        ('loan', 'Loan'),
        ('income', 'Income'),
        ('returns', 'returns')
    )
LOGIN_REDIRECT_URL = '/'
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_HTTPONLY=False