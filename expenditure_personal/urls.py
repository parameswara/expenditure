"""expenditure_personal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('load_amount.urls', namespace='index')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('listall/', include('all_view.urls', namespace='all_view')),
    path('expense/', include('load_amount.urls', namespace='amount')),
    path('income/', include('income.urls', namespace='income')),
    path('chain/', include('chain.urls', namespace='chain')),
    path('loan/', include('loan.urls', namespace='loan')),
    path('returns/', include('returns.urls', namespace='returns')),
    path('reversal/', include('reversal.urls', namespace='reversal')),
    path('credits/', include('user.urls', namespace='credits')),
    path('expwith/', include('cashwithdrawl.urls', namespace='expwith'))
]
