"""
WSGI config for expenditure_personal project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys
sys.path.append('/opt/code/expenditure')

from django.core.wsgi import get_wsgi_application

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'expenditure_personal.settings')
os.environ['DJANGO_SETTINGS_MODULE'] = 'expenditure_personal.settings'
application = get_wsgi_application()
