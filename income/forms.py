from django import forms
from .models import Income


class IncomeForm(forms.ModelForm):
    class Meta:
        model = Income
        fields = ['credited_by','income_got', 'income_source', 'description', 'income_date', 'credited_to']
