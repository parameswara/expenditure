from django.db import models
from django.conf import settings
# Create your models here.

class Income(models.Model):
    credited_by = models.CharField(max_length=10, default=None, null=True)
    income_got = models.PositiveIntegerField()
    income_source = models.CharField(max_length=10, choices=settings.GET_MONEY, default=settings.GET_MONEY[1][0])
    description = models.CharField(max_length=200, default=None, null=True)
    income_date = models.DateField()
    credited_to = models.CharField(max_length=10, choices=settings.BANKS, default=settings.BANKS[1][0])

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)
