from django.urls import path
from . import views

app_name = 'income'

urlpatterns = [
    path('', views.income_data, name='get_data_income'),
    path('load_income/',views.form_data, name='load_income'),
    path('load_income/<int:id>/', views.update, name='update_load_income'),
    path('dropdown/', views.income_dropdown, name='dropdown')
]