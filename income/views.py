from django.shortcuts import render
from .models import Income
from .forms import IncomeForm
from load_amount.models import Money
from chain.models import ChainModel
from cashwithdrawl.models import CashWithdrawl
from returns.models import Returns
from django.http import HttpResponseRedirect, HttpResponse
from all_view.views import all_list_rest
from django.contrib.auth.decorators import login_required
from user.models import UserProfile
from django.http import JsonResponse
# Create your views here.


def income_data(request):
    all_data = Income.objects.all()
    return render(request, 'app/income_index.html', {'posts':all_data})


@login_required
def form_data(request):
    user = UserProfile.objects.get(user_id=request.user.id)
    if request.method == "POST":
        form = IncomeForm(request.POST)
        if form.is_valid():
            user.commits += 1
            user.save()
            form.save()
            return HttpResponseRedirect('/listall')
    else:
        form = IncomeForm()
    income_form, load_form, chain_form, loan_form, returns_form = all_list_rest()
    return render(request,'app/load_income.html',{'form':form, 'income_form':income_form, 'load_form':load_form,
                                                  'chain_form':chain_form, 'loan_form': loan_form,'returns_form':returns_form})

def update(request, id):
    if request.method == "POST":
        data = Income.objects.get(id=id)
        data.credited_by = request.POST['credited_by']
        data.income_got = request.POST['income_got']
        data.income_source = request.POST['income_source']
        data.description = request.POST['description']
        data.income_date = request.POST['income_date']
        data.credited_to = request.POST['credited_to']
        data.save()
        return HttpResponse('updated')
    elif request.method == "GET":
        balance = 0
        data = Income.objects.get(id=id)
        initial_balance = data.income_got
        expense = Money.objects.filter(income_link=id)
        for each_expense_money in expense:
            if Returns.objects.filter(return_link=each_expense_money.id).exists():
                ret = Returns.objects.filter(return_link=each_expense_money.id)
                for each_retrun in ret:
                    if each_retrun.income_id is not None:
                        balance += int(each_expense_money.money_spent)
            else:
                balance += int(each_expense_money.money_spent)
        expense = ChainModel.objects.filter(income_chain=id)
        for each_expense_money in expense:
            balance += int(each_expense_money.money_spent)
        expense = CashWithdrawl.objects.filter(income_link=id)
        for each_expense_money in expense:
            balance += int(each_expense_money.money_spent)

        return JsonResponse({'data': int(initial_balance - balance)})

def income_dropdown(request):
    all_data = []
    all_data_object = Income.objects.all()
    for each_withdrawl in all_data_object:
        temp = {'id': str(each_withdrawl.id), 'desp':str(each_withdrawl.id) + ' ' + str(each_withdrawl.description)}
        all_data.append(temp)
    return JsonResponse({'data': all_data})