from django.apps import AppConfig

class LoadAmountConfig(AppConfig):
    name = 'load_amount'
