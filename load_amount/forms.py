from django import forms
from .models import Money


class ExpenseForm(forms.ModelForm):
    class Meta:
        model = Money
        fields = ['money_spent', 'money_source', 'expense_category', 'description', 'purchase_date', 'expense_link', 'income_link', 'chain_link', 'loan_link', 'withdraw_link']

