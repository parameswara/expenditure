 # Generated by Django 2.1.5 on 2019-02-24 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('load_amount', '0005_auto_20190224_1906'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='money',
            options={'ordering': ('-id',)},
        ),
        migrations.AlterField(
            model_name='money',
            name='expense_link',
            field=models.CharField(choices=[('income', 'Income'), ('chain', 'Chain'), ('loan', 'Loan')], default='income', max_length=10),
        ),
    ]
