# Generated by Django 2.1.5 on 2019-03-03 11:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('returns', '0002_returns_credited_to'),
        ('load_amount', '0008_auto_20190227_0728'),
    ]

    operations = [
        migrations.AddField(
            model_name='money',
            name='returns_link',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='reverse_returns_link', to='returns.Returns'),
        ),
    ]
