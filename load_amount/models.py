from django.db import models
from income.models import Income
from chain.models import ChainModel
from loan.models import loan
from django.conf import settings
# Create your models here.


class Money(models.Model):
    loan_link = models.ForeignKey('loan.loan', on_delete=models.CASCADE, related_name='loan_related',
                                   default=None, null=True, blank=True)
    chain_link = models.ForeignKey('chain.ChainModel', on_delete=models.CASCADE, related_name='chain_income',
                                   default=None, null=True, blank=True)
    income_link = models.ForeignKey('income.Income', on_delete=models.CASCADE, related_name='income', default=None,
                                    null=True, blank=True)
    returns_link = models.ForeignKey('returns.Returns', on_delete=models.CASCADE, related_name='reverse_returns_link',
                                     default=None,
                                     null=True, blank=True)
    withdraw_link = models.ForeignKey('cashwithdrawl.CashWithdrawl', on_delete=models.CASCADE, related_name='cashwithdrawl_link',
                                     default=None,
                                     null=True, blank=True)
    expense_link = models.CharField(max_length=10, choices=settings.SOURCE, default=settings.SOURCE[0][0], null=True, blank=True)
    money_spent = models.PositiveIntegerField()
    money_source = models.CharField(max_length=10, choices=settings.BANKS, default=settings.BANKS[1][0], null=True, blank=True)
    expense_category = models.CharField(max_length=20, choices=settings.CATEGORY, default=settings.CATEGORY[3][0])
    description = models.CharField(max_length=200, default=None, null=True)
    purchase_date = models.DateField()

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)

# class ChainMoney(models.Model):

