from django.urls import path
from . import views

app_name = 'load_amount'

urlpatterns = [
    path('', views.index, name='get_data'),
    path('load_expense/', views.form_data, name='load_expense'),
    path('load_expense/<int:id>/', views.update, name='load_expense')
]
