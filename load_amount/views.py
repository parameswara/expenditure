from django.shortcuts import render
from .models import Money
from .forms import ExpenseForm
from user.models import UserProfile
from django.http import HttpResponseRedirect, HttpResponse
from all_view.views import all_list_rest
from django.contrib.auth.decorators import login_required
from cashwithdrawl.forms import CashWithdrawlForm
from django.http import JsonResponse
from django.http import QueryDict

# Create your views here.
@login_required
def index(request):
    return HttpResponseRedirect('index')

def get_data(request):
    all_data = Money.objects.all()
    return render(request, 'app/load_index.html', {'posts':all_data})

# @login_required
def form_data(request):
    # request.user.id = 1
    user = UserProfile.objects.get(user_id=request.user.id)
    if request.method == "POST":
        category = request.POST.get('expense_category')
        if category == "cash_with":
            req_copy = request.POST.copy()
            user_dict = {'user': request.user.id}
            req_copy.update(user_dict)
            form = CashWithdrawlForm(req_copy)

            if form.is_valid():
                form.save()
                user.commits += 1
                user.save()
                return JsonResponse({"status": 200, "message":"successfully uploaded"})
        else:
            form = ExpenseForm(request.POST)
            if form.is_valid():
                form.save()
                user.commits += 1
                user.save()
                return JsonResponse({"status": 200, "message":"successfully uploaded"})
    else:
        form = ExpenseForm()
    income_form, load_form, chain_form, loan_form, returns_form = all_list_rest()
    return render(request,'app/load_expense.html',{'form':form, 'income_form':income_form, 'load_form':load_form,
                                                   'chain_form':chain_form, 'loan_form': loan_form,'returns_form':returns_form})


def update(request, id):
    data = Money.objects.get(id=id)
    data.money_spent = request.POST['money_spent']
    data.money_source = request.POST['money_source']
    data.expense_category = request.POST['expense_category']
    data.description = request.POST['description']
    data.purchase_date = request.POST['purchase_date']
    data.save()
    return JsonResponse({"status": 200, "message":"successfully updated"})