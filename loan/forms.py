from .models import loan
from django import forms

class LoanForm(forms.ModelForm):
    class Meta:
        model = loan
        fields = ['credited_by', 'credited_to', 'loan_got', 'loan_tenure', 'description', 'loan_date']
