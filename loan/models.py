from django.db import models
from django.conf import settings
# Create your models here.

class loan(models.Model):
    credited_by = models.CharField(max_length=10, default=None, null=True)
    credited_to = models.CharField(max_length=10, choices=settings.BANKS, default=settings.BANKS[2][0])
    loan_got = models.PositiveIntegerField()
    loan_tenure = models.PositiveIntegerField()
    description = models.CharField(max_length=200, default=None, null=True)
    loan_date = models.DateField(default=None, null=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)
