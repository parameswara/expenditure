from django.urls import path
from .views import loan_data, update, loan_dropdown

app_name = 'loan'

urlpatterns = [
    path('', loan_data, name='loan_data'),
    path('loans/<int:id>/', update, name='update_loan_data'),
    path('dropdown/', loan_dropdown, name='dropwn_loan')
]