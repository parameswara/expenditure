from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import LoanForm
from .models import loan
from load_amount.models import Money
from chain.models import ChainModel
from cashwithdrawl.models import CashWithdrawl
from returns.models import Returns
from user.models import UserProfile
from all_view.views import all_list_rest
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
# Create your views here.

@login_required
def loan_data(request):
    user = UserProfile.objects.get(user_id=request.user.id)
    if request.method == "POST":
        form = LoanForm(request.POST)
        if form.is_valid():
            user.commits += 1
            user.save()
            form.save()
            return JsonResponse({"status": 200, "message":"successfully uploded"})
    else:
        form = LoanForm()
    income_form, load_form, chain_form, loan_form, returns_form = all_list_rest()
    return render(request,'app/loan.html',{'form':form, 'income_form':income_form, 'load_form':load_form,
                                           'chain_form':chain_form, 'loan_form': loan_form, 'returns_form':returns_form})

def update(request, id):
    if request.method == "POST":
        data = loan.objects.get(id=id)
        data.credited_by = request.POST['credited_by']
        data.credited_to = request.POST['credited_to']
        data.loan_got = request.POST['loan_got']
        data.loan_tenure = request.POST['loan_tenure']
        data.description = request.POST['description']
        data.loan_date = request.POST['loan_date']
        data.save()
        return JsonResponse({"status": 200, "message":"successfully updated"})
    elif request.method == "GET":
        balance = 0
        data = loan.objects.get(id=id)
        initial_balance = data.loan_got
        expense = Money.objects.filter(loan_link=id)
        for each_expense_money in expense:
            if Returns.objects.filter(return_link=each_expense_money.id).exists():
                ret = Returns.objects.filter(return_link=each_expense_money.id)
                for each_retrun in ret:
                    if each_retrun.income_id is not None:
                        balance += int(each_expense_money.money_spent)
            else:
                balance += int(each_expense_money.money_spent)
        expense = CashWithdrawl.objects.filter(loan_link=id)
        for each_expense_money in expense:
            balance += int(each_expense_money.money_spent)

        return JsonResponse({'data': int(initial_balance - balance)})

def loan_dropdown(request):
    all_data = []
    all_data_object = loan.objects.all()
    for each_withdrawl in all_data_object:
        temp = {'id': str(each_withdrawl.id), 'desp':str(each_withdrawl.id) + ' ' +str(each_withdrawl.description)}
        all_data.append(temp)
    return JsonResponse({'data': all_data})
