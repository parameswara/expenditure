from django import forms
from .models import Returns

class ReturnForm(forms.ModelForm):
    class Meta:
        model = Returns
        fields = ['amount', 'description', 'purchase_date', 'return_link', 'credited_to', 'income_id']