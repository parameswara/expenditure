# Generated by Django 2.1.5 on 2019-02-26 16:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('load_amount', '0007_auto_20190226_2058'),
    ]

    operations = [
        migrations.CreateModel(
            name='Returns',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.PositiveIntegerField()),
                ('description', models.CharField(default=None, max_length=200, null=True)),
                ('purchase_date', models.DateField()),
                ('return_link', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='returns', to='load_amount.Money')),
            ],
            options={
                'ordering': ('-id',),
            },
        ),
    ]
