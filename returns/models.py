from django.db import models
from load_amount.models import Money
from django.conf import settings
# Create your models here.


class Returns(models.Model):
    return_link = models.ForeignKey('load_amount.Money', on_delete=models.CASCADE, related_name='returns', default=None,
                                    null=True, blank=True)
    amount = models.PositiveIntegerField()
    description = models.CharField(max_length=200, default=None, null=True)
    purchase_date = models.DateField()
    credited_to = models.CharField(max_length=10, choices=settings.BANKS, default=None, null=True)
    income_id = models.PositiveIntegerField(default=None, null=True, blank=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)
