from django.urls import path
from . import views


app_name = 'returns'

urlpatterns = [
    path('', views.returns_view, name='returns'),
    path('returns/<int:id>/', views.update, name='update_returns'),
]