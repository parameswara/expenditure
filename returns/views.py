from django.shortcuts import render
from .forms import ReturnForm
from .models import Returns
from django.http import JsonResponse
from all_view.views import all_list_rest
from load_amount.models import Money
from income.models import Income
from django.contrib.auth.decorators import login_required
from user.models import UserProfile
# Create your views here.

@login_required
def returns_view(request):
    user = UserProfile.objects.get(user_id=request.user.id)
    if request.method == "POST":
        post_values = request.POST.copy()
        expenditure_data = Money.objects.get(id=request.POST['return_link'])
        if expenditure_data.money_source != request.POST['credited_to']:
            new_income = Income.objects.create(credited_by='returns',
                                               income_got=request.POST['amount'],
                                               income_source='returns',
                                               description=request.POST['description'],
                                               income_date=request.POST['purchase_date'],
                                               credited_to=request.POST['credited_to'])
            new_income.save()
            post_values['income_id'] = str(new_income.id)
        form = ReturnForm(post_values)
        if form.is_valid():
            user.commits += 1
            user.save()
            form.save()
            return JsonResponse({"status": 200, "message":"successfully uploded"})
    else:
        form = ReturnForm()
    income_form, load_form, chain_form, loan_form, returns_form = all_list_rest()
    return render(request,'app/returns.html',{'form':form, 'income_form':income_form, 'load_form':load_form,
                                              'chain_form':chain_form, 'loan_form': loan_form,
                                              'returns_form':returns_form})


def update(request, id):
    data = Returns.objects.get(id=id)
    data.amount = request.POST['amount']
    data.credited_to = request.POST['credited_to']
    data.description = request.POST['description']
    data.income_id = request.POST['income_id']
    data.return_link = request.POST['return_link']
    data.purchase_date = request.POST['purchase_date']
    data.save()
    return JsonResponse({"status": 200, "message":"successfully updated"})