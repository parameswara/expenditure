from django.apps import AppConfig


class ReversalConfig(AppConfig):
    name = 'reversal'
