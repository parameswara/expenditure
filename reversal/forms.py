from django import forms
from .models import Reversal

class ReversalForm(forms.ModelForm):
    class Meta:
        model = Reversal
        fields = ['reversal_amount', 'source_link', 'chain_link', 'loan_link', 'income_link', 'returns_link',
                  'description', 'purchase_date', 'reversal_loan']