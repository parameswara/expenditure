from django.db import models
from django.conf import settings
# Create your models here.

class Reversal(models.Model):

    chain_link = models.ForeignKey('chain.ChainModel', on_delete=models.CASCADE, related_name='reverse_chain_link', default=None,
                                    null=True, blank=True)
    loan_link = models.ForeignKey('loan.loan', on_delete=models.CASCADE, related_name='reverse_loan_link', default=None,
                                    null=True, blank=True)
    income_link = models.ForeignKey('income.Income', on_delete=models.CASCADE, related_name='reverse_income_link', default=None,
                                  null=True, blank=True)
    returns_link = models.ForeignKey('returns.Returns', on_delete=models.CASCADE, related_name='reversal_returns_link', default=None,
                                  null=True, blank=True)
    reversal_amount = models.PositiveIntegerField()
    source_link = models.CharField(max_length=10, choices=settings.SELECT_LINK, default=settings.SELECT_LINK[1][0])
    description = models.CharField(max_length=200, default=None, null=True)
    purchase_date = models.DateField()
    reversal_loan = models.ForeignKey('loan.loan', on_delete=models.CASCADE, related_name='reverse_reversal_loan_link', default=None,
                                  null=True, blank=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.id)