from django.urls import path
from . import views

app_name = 'reversal'

urlpatterns = [
    path('', views.form_data, name='reversal'),
    path('reversal/<int:id>/', views.update, name='update_reversal'),
]