from django.shortcuts import render
from .forms import ReversalForm
from .models import Reversal
from user.models import UserProfile
from django.http import JsonResponse
from all_view.views import all_list_rest
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def form_data(request):
    user = UserProfile.objects.get(user_id=request.user.id)
    if request.method == "POST":
        form = ReversalForm(request.POST)
        if form.is_valid():
            user.commits += 1
            user.save()
            form.save()
            return HttpResponseRedirect('/listall')
    else:
        form = ReversalForm()
    income_form, load_form, chain_form, loan_form, returns_form = all_list_rest()
    return render(request,'app/reversal.html',{'form':form, 'income_form':income_form, 'load_form':load_form,
                                                   'chain_form':chain_form, 'loan_form': loan_form,'returns_form':returns_form})


def update(request, id):
    data = Reversal.objects.get(id=id)
    data.reversal_amount = request.POST['reversal_amount']
    data.purchase_date = request.POST['purchase_date']
    data.description = request.POST['description']
    data.save()
    return HttpResponse('updated')