
from .models import UserProfile
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def credits(request):
    user = UserProfile.objects.get(user_id=request.user.id)
    commits = user.commits
    return JsonResponse({'commits': int(commits), 'username':str(request.user.username)})
