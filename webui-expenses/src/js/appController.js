/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'base/windows_location', 'models/credits', 'models/exp_with', 'models/balance', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource', 'ojs/ojdialog', 'ojs/ojinputtext',  'ojs/ojdatetimepicker',
             'ojs/ojselectcombobox', 'ojs/ojtimezonedata'],
        function (oj, ko, location, credits, withdrawl, balance) {
            function ControllerViewModel() {
                var self = this;

                // Media queries for repsonsive layouts
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                var base = new location.windowsVM();
                var baseurl = base.baseurl;
                
                

                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.configure({
                    'dashboard': {label: 'Dashboard', isDefault: true},
                    'customers': {label: 'Customers'},
                    'about': {label: 'About'}
                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

                // Navigation setup
                var navData = [
                    {name: 'Dashboard', id: 'dashboard',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},
                    {name: 'Reports', id: 'customers',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-people-icon-24'},
                    {name: 'Edit', id: 'about',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-info-icon-24'}
                ];
                self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

                // Header
                // Application Name used in Branding Area
                self.appName = ko.observable("Finance");
                self.credits = ko.observable();
                self.userLogin = ko.observable();
                self.money_spent = ko.observable();
                self.money_source = ko.observable('h_c');
                self.expense_category = ko.observable('groceries');
                self.description = ko.observable();
                self.purchase_date = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));
                self.expense_link_status = ko.observable(' ');
                self.withdrawl_link = ko.observable();
                self.selectedMenuItem = ko.observable("");
                self.expense_withdrawl = ko.observable();
                self.income_link = ko.observable("");
                self.chain_link = ko.observable("");
                self.loan_link = ko.observable("");
                
                
                self.expense_cat = [{value: 'food', label: 'Food'},
                    {value: 'cloth', label: 'Cloth'},
                    {value: 'petrol', label: 'Petrol'},
                    {value: 'groceries', label: 'Groceries'},
                    {value: 'veggies', label: 'Veggies'},
                    {value: 'petrol', label: 'Petrol'},
                    {value: 'entertainment', label: 'Entertainment'},
                    {value: 'medical', label:'medical'},
                    {value: 'interest', label: 'Interest'},
                    {value: 'bank_charges', label: 'Bank Charges'},
                    {value: 'phone_charges', label: 'Phone Charges'},
                    {value: 'home_internet', label: 'Home Internet'},
                    {value: 'travel', label: 'Travel'},
                    {value: 'electric', label: 'Electrical'},
                    {value: 'loan_given', label: 'Hand Loan'},
                    {value: 'education', label: 'Education'},
                    {value: 'others', label: 'others'}

                ]
                
                
                self.tasksVM_credits = new credits.creditVM();
                self.tasksVM_credits.init_credits();
                
                
                // User Info used in Global Navigation area
               self.tasksVM_credits.fetch(function (credits_collection, response, options) {
                            self.userLogin(response.username);
                            self.credits(response.commits);
                        });
                        
                self.expwith = new withdrawl.withexpVM();
                self.expwith.exp_credits("expwith");
                
                self.expwith.fetch(function (expense_collection, response, options) {
                        self.expense_withdrawl(response.data);
                    });
                
                
                self.login_preference = function (event) {
                     self.tasksVM_credits.fetch(function (credits_collection, response, options) {
                            self.userLogin(response.username);
                            self.credits(response.commits);
                        });
                    $("#modalDialog1").ojDialog("open");
                };
                
                self.quick_expense = function (event) {
                    self.expwith.fetch(function (expense_collection, response, options) {
                        self.expense_withdrawl(response.data);
                    });
                    
                    $("#modalDialog2").ojDialog("open");
                };

                self.close = function (event) {
                    $("#modalDialog1").ojDialog("close");
                };

                //                genterate balance left
                self.withdrawbalance = ko.observable();
                self.withdrawbalancelink = new balance.balanceVM();
             
                self.withdrawl_link.subscribe(function () {
                    self.withdrawbalancelink.balance_credits("expwith/" + self.withdrawl_link())
                    self.withdrawbalancelink.fetch(function (expense_collection, response, options) {
                            self.withdrawbalance(response.data);
                        });
                    
                });

                // Footer
                function footerLink(name, id, linkTarget) {
                    this.name = name;
                    this.linkId = id;
                    this.linkTarget = linkTarget;
                }
                self.footerLinks = ko.observableArray([
                    new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                    new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                    new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                    new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                    new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
                ]);
                
                self.close_exp = function (event) {
                    var data;
                    data = new FormData();
                    data.append('money_spent', self.money_spent());
                    data.append('money_source', self.money_source());
                    data.append('expense_category', self.expense_category());
                    data.append('description', self.description());
                    data.append('purchase_date', self.purchase_date());
                    data.append('expense_link', self.expense_link_status());
                    data.append('income_link', self.income_link());
                    data.append('chain_link', self.chain_link());
                    data.append('loan_link', self.loan_link());
                    data.append('withdraw_link', self.withdrawl_link());

                    console.log(data);

                    $.ajax({
                        url: 'http://'+ baseurl + '/expense/load_expense/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert(data);
                        }
                    });

                    self.money_spent('');
                    self.money_source('h_c');
                    self.expense_category('groceries');
                    self.description('');
                    self.expense_link_status(' ');
                    self.income_link('');
                    self.chain_link('');
                    self.loan_link('');
                    
                    $("#modalDialog2").ojDialog("close");
                   
                
                    return true;
                }
            }

            return new ControllerViewModel();
        }
);
