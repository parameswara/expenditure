/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(['ojs/ojcore', 'knockout', 'jquery'],
        function (oj, ko, $) {
            function WindowsViewModel() {
                var self = this;
                var hostname = window.location.hostname;
  
                if (hostname == '127.0.0.1') {
                    self.baseurl = "127.0.0.1:3333";
                } else {
                    self.baseurl = hostname;
                }
            }
            return {"windowsVM": WindowsViewModel};
            });


