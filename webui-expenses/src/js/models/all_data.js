/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'base/windows_location', "ojs/ojmodel"],
        function (oj, ko, $, location) {
            function viewModel() {
                var self = this;
                self.taskCol = ko.observable();
                var base = new location.windowsVM();
                var baseurl = base.baseurl;


                self.upload = function (successCallBack, failureCallBack, data) {
                    var ajaxOptions = {
                        "async": true,
                        "crossDomain": false,
                        "method": "POST",
                        "processData": false,
                        "contentType": false,
                        "mimeType": "multipart/form-data",
                        "data": data,
                        "success": successCallBack,
                        "error": failureCallBack,
                        "xhrFields": {
                            withCredentials: true
                        }

                    };
                    $.ajax(ajaxOptions);
                };


                self.EmpDef = oj.Model.extend({

                    url: "http://" + baseurl + "/listall/get_data/",

                    idAttribute: "id"

                });

                self.EmpsDef = oj.Collection.extend({

                    url: "http://" + baseurl + "/listall/get_data/",

                    model: new self.EmpDef,

                    comparator: "id"

                });

                var income = new self.EmpsDef;

                self.fetch = function (successCallBack, failureCallback, options) {
                    // populate the collection by calling fetch()
                    var fetchOptions = {
                        success: successCallBack,
                        error: failureCallback
                    };
                    if (options && options.data) {
                        fetchOptions.data = options.data;
                    }
                    self.taskCol().fetch(fetchOptions);
                };

                self.init_income = function (method) {
                    self.taskCol(income);
                    self.taskCol().url= "http://"+ baseurl + "/listall/get_data/" + "?method=" + method;
                };
                
                self.init_expense = function (method) {
                    self.taskCol(income);
                    self.taskCol().url= "http://" + baseurl + "/listall/get_data/" + "?method=" + method;
                };
            }
            return {"taskVM": viewModel};
        });


