/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['ojs/ojcore', 'knockout', 'jquery', 'base/windows_location', "ojs/ojmodel"],
        function (oj, ko, $, location) {
            function viewModel() {
                var self = this;
                self.taskCol = ko.observable();
                var base = new location.windowsVM();
                var baseurl = base.baseurl;
                
                self.with_def = oj.Model.extend({

                    url: "http://" + baseurl + "/expwith/",

                    idAttribute: "commits"

                });

                self.EmpsDef = oj.Collection.extend({

                    url: "http://" + baseurl + "/expwith/",

                    model: new self.with_def,

                    comparator: "commits"

                });
                
                 var credits_data = new self.EmpsDef;

                self.fetch = function (successCallBack, failureCallback, options) {
                    // populate the collection by calling fetch()
                    var fetchOptions = {
                        success: successCallBack,
                        error: failureCallback,
                    };
                    if (options && options.data) {
                        fetchOptions.data = options.data;
                    }
                    self.taskCol().fetch(fetchOptions);
                };
                
                self.balance_credits = function (method) {
                    self.taskCol(credits_data);
                    self.taskCol().url= "http://" + baseurl + "/" + method + "/";
                };

            }
            return {"balanceVM": viewModel};
        });
