/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'models/all_data', 'base/windows_location', 'ojs/ojknockout', 'promise', 'ojs/ojmodel', 'ojs/ojknockout-model',
    'ojs/ojtable', 'ojs/ojdatacollection-utils', 'ojs/ojinputtext', 'ojs/ojvalidation-datetime', 'ojs/ojvalidation-number', 'ojs/ojcollectiontabledatasource',
    'ojs/ojcollectiontabledatasource'],
        function (oj, ko, $, all_data, location) {

            function AboutViewModel() {
                var self = this;
                self.DeptCol = ko.observable();
                self.incomedatasource = ko.observable();
                self.expensedatasource = ko.observable();
                self.chaindatasource = ko.observable();
                self.loansdatasource = ko.observable();
                self.returnsdatasource = ko.observable();
                self.reversaldatasource = ko.observable();
                self.method_type = ko.observable('income');
                self.tasksVM_income = new all_data.taskVM();
                self.tasksVM_expense = new all_data.taskVM();
                self.tasksVM_chain = new all_data.taskVM();
                self.tasksVM_loans = new all_data.taskVM();
                self.tasksVM_returns = new all_data.taskVM();
                self.tasksVM_reversal = new all_data.taskVM();
                self.selectedItem = ko.observable("income");
                self.currentEdge = ko.observable("top");
                
                self.tasksVM_income.init_income("income");
                self.tasksVM_income.fetch(function (income_collection, response, options) {
                    self.incomedatasource(new oj.CollectionTableDataSource(income_collection));
                });

                
                self.selectedItem.subscribe(function () {
                    if (self.selectedItem() == 'income') {
                        self.tasksVM_income.init_income("income");
                        self.tasksVM_income.fetch(function (income_collection, response, options) {
                            self.incomedatasource(new oj.CollectionTableDataSource(income_collection));
                        });
                       
                    } else if (self.selectedItem() == 'expense') {
                        self.tasksVM_expense.init_expense("expense");
                        self.tasksVM_expense.fetch(function (expense_collection, expense_response, expense_options) {
                            self.expensedatasource(new oj.CollectionTableDataSource(expense_collection));
                        });
                    } else if (self.selectedItem() == 'chain') {
                        self.tasksVM_chain.init_expense("chain");
                        self.tasksVM_chain.fetch(function (chain_collection, expense_response, expense_options) {
                            self.chaindatasource(new oj.CollectionTableDataSource(chain_collection));
                        });
                    } else if (self.selectedItem() == 'loans') {
                        self.tasksVM_loans.init_expense("loans");
                        self.tasksVM_loans.fetch(function (loans_collection, expense_response, expense_options) {
                            self.loansdatasource(new oj.CollectionTableDataSource(loans_collection));
                        });
                    } else if (self.selectedItem() == 'returns') {
                        self.tasksVM_returns.init_expense("returns");
                        self.tasksVM_returns.fetch(function (returns_collection, expense_response, expense_options) {
                            self.returnsdatasource(new oj.CollectionTableDataSource(returns_collection));
                        });
                    } else if (self.selectedItem() == 'reversal') {
                        self.tasksVM_reversal.init_expense("reversal");
                        self.tasksVM_reversal.fetch(function (reversal_collection, expense_response, expense_options) {
                            self.reversaldatasource(new oj.CollectionTableDataSource(reversal_collection));
                        });
                    }
                });


                // function to determine which template to use for 
                // rendering depending on mode
                self.incomeRowTemplate = function (data, context)
                {
                    var mode = context.$rowContext['mode'];

                    if (mode === 'edit')
                    {
                        return 'editRowTemplate';
                    } else if (mode === 'navigation')
                    {
                        return 'rowTemplate';
                    }
                };
                
                self.expenseRowTemplate = function (data, context2)
                {
                    var mode2 = context2.$rowContext['mode'];

                    if (mode2 === 'edit')
                    {
                        return 'expenserowTemplate';
                    } else if (mode2 === 'navigation')
                    {
                        return 'expenseeditRowTemplate';
                    }
                };
                
                self.chainRowTemplate = function (data, context3)
                {
                    var mode3 = context3.$rowContext['mode'];

                    if (mode3 === 'edit')
                    {
                        return 'chainrowTemplate';
                    } else if (mode3 === 'navigation')
                    {
                        return 'chaineditRowTemplate';
                    }
                };
                
                self.loansRowTemplate = function (data, context4)
                {
                    var mode4 = context4.$rowContext['mode'];

                    if (mode4 === 'edit')
                    {
                        return 'loansrowTemplate';
                    } else if (mode4 === 'navigation')
                    {
                        return 'loanseditRowTemplate';
                    }
                };
                
                self.returnsRowTemplate = function (data, context5)
                {
                    var mode5 = context5.$rowContext['mode'];

                    if (mode5 === 'edit')
                    {
                        return 'returnsrowTemplate';
                    } else if (mode5 === 'navigation')
                    {
                        return 'returnseditRowTemplate';
                    }
                };
                
                self.reversalRowTemplate = function (data, context6)
                {
                    var mode6 = context6.$rowContext['mode'];

                    if (mode6 === 'edit')
                    {
                        return 'reversalrowTemplate';
                    } else if (mode6 === 'navigation')
                    {
                        return 'reversaleditRowTemplate';
                    }
                };

                //// NUMBER AND DATE CONVERTER ////
                var numberConverterFactory = oj.Validation.converterFactory("number");
                this.numberConverter = numberConverterFactory.createConverter();

                var dateConverterFactory = oj.Validation.converterFactory("datetime");
                this.dateConverter = dateConverterFactory.createConverter();
                
                
                };
            var vm = new AboutViewModel();
            var base = new location.windowsVM();
            var baseurl = base.baseurl;
            
            function beforeRowEditEndListener(event, data)
            {
                var rowIdx = data.rowContext.status.rowIndex;
                var sample = oj.KnockoutUtils.map(data.rowContext.datasource.data, null, true)
                var another_sample  = sample()[rowIdx]["oj._internalObj"]
                
                
                vm.incomedatasource().at(rowIdx).then(function (rowObj)
                {
                    var keys_data = another_sample.keys();
                    var values_data = another_sample.values();
                    data = new FormData();
                    var length_data = Object.keys(keys_data).length;
                    console.log(vm.selectedItem())
                    if (keys_data.includes("income_got")) {
                        var method = "income/load_income/";
                    } else if (keys_data.includes("money_spent")) {
                        var method = "expense/load_expense/";
                    } else if (keys_data.includes("money_spent")) {
                        var method = "chain/chain/";
                    } else if (keys_data.includes("loan_got")) {
                        var method = "loans/loans/";
                    } else if (keys_data.includes("income_id")) {
                        var method = "returns/returns/";
                    } else if (keys_data.includes("returns_link")) {
                        var method = "reversal/reversal/";
                    }
                    
                    for (var i=0; i<=length_data - 1 ; i++){
                        data.append(keys_data[i], values_data[i]);
                    }

                    $.ajax({
                        url: 'http://'+ baseurl + method + values_data[0] + '/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert(data);
                        }
                    });
    
                });
                return oj.DataCollectionEditUtils.basicHandleRowEditEnd(event, data);
            };
//
            $(document).ready
                    (
                            function ()
                            {
                                $('#table').on('ojbeforeroweditend', beforeRowEditEndListener);
                                $('#table2').on('ojbeforeroweditend', beforeRowEditEndListener);
                                $('#table3').on('ojbeforeroweditend', beforeRowEditEndListener);
                                $('#table4').on('ojbeforeroweditend', beforeRowEditEndListener);
                                $('#table5').on('ojbeforeroweditend', beforeRowEditEndListener);
                                $('#table6').on('ojbeforeroweditend', beforeRowEditEndListener);
                            }
                    );
           
            
            // Below are a subset of the ViewModel methods invoked by the ojModule binding
            // Please reference the ojModule jsDoc for additional available methods.

            /**
             * Optional ViewModel method invoked when this ViewModel is about to be
             * used for the View transition.  The application can put data fetch logic
             * here that can return a Promise which will delay the handleAttached function
             * call below until the Promise is resolved.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
             * the promise is resolved
             */


            self.handleActivated = function (info) {
                // Implement if needed
            };

            /**
             * Optional ViewModel method invoked after the View is inserted into the
             * document DOM.  The application can put logic that requires the DOM being
             * attached here.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
             */
            self.handleAttached = function (info) {
                // Implement if needed
            };


            /**
             * Optional ViewModel method invoked after the bindings are applied on this View.
             * If the current View is retrieved from cache, the bindings will not be re-applied
             * and this callback will not be invoked.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             */
            self.handleBindingsApplied = function (info) {
                
               
            };
            /*
             * Optional ViewModel method invoked after the View is removed from the
             * document DOM.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
             */
            self.handleDetached = function (info) {
                
              
                // Implement if needed
            };


            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new AboutViewModel();
        }
);
