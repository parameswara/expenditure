/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your customer ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'models/all_data', 'ojs/ojknockout', 'ojs/ojmodel', 'ojs/ojtable', 'ojs/ojcollectiontabledatasource',  'ojs/ojchart',
    'ojs/ojbutton', 'ojs/ojtoolbar', 'ojs/ojselectcombobox'],
        function (oj, ko, $, all_data) {

            function CustomerViewModel() {
                var self = this;

                self.expensedatasourcetemp = ko.observableArray();
                self.threeDValue = ko.observable('off');
                self.viewmode = ko.observable('pie');
                self.hiddenCategories = ko.observableArray([]);
                self.method = ko.observable('expense');
                

                self.tasksVM_expense_catageory = new all_data.taskVM();
                
//                tags

                self.tags = ko.observableArray([
                    {value: 'no', label: 'None'}
                ]);
                

//                Button functions

                self.months_array = ko.observableArray(["01"]);
                self.months_array_belt = ko.observableArray(["01"]);
                self.year_array = ko.observable("2019");
                
                self.year_values = [
                    {id: '2016', label: '2016'},
                    {id: '2017', label: '2017'},
                    {id: '2018', label: '2018'},
                    {id: '2019', label: '2019'},
                ];
                
                self.view_values = [
                    {id: 'pie', label: 'PIE'},
                    {id: 'bar', label: 'BAR'},
                    {id: 'tab', label: 'TABLE'}
                ];
                
                self.method_values = [
                    {id: 'income', label: 'INCOME'},
                    {id: 'expense', label: 'EXPENSE'},
                    {id: 'loans', label: 'LOANS'},
                    {id: 'returns', label: 'RETURNS'}
                ];

                // use that (entire) observable array:
                self.classes = ko.computed(function () {
                    return self.months_array().join(",");
                }, self);

                // illustrates the app setting the original observable:
                self.toggleAll = function () {
                    var allToggled = self.months_array().length == 12;
                    self.months_array(allToggled ? ["01"] : ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]);
                };

                self.months_array.subscribe(function () {
                    var arg = self.method() + "&filter=category" + "&year=" + self.year_array() + "&month=" + self.classes() + "&view=" + self.viewmode();
                    self.tasksVM_expense_catageory.init_income(arg);
                    self.tasksVM_expense_catageory.fetch(function (expense_collection, response, options) {
                        self.expensedatasourcetemp(response.data);
                    });
                    $("#formatset").ojButtonset("refresh");
                });
                
                $(document).ready(function () {
                    $("#bar-chart-container").hide();
                });
                
                self.viewmode.subscribe(function (){
                    if (self.viewmode() == "pie") {
                        $("#pie-chart-container").show();
                        $("#bar-chart-container").hide();
                    } else if (self.viewmode() == "bar") {
                        $("#pie-chart-container").hide();
                        $("#bar-chart-container").show();
                    }
                    $("#formatset").ojButtonset("refresh");
                });


                /* toggle button variables */
                self.stackValue = ko.observable('off');
                self.orientationValue = ko.observable('vertical');

                /* chart data */
               
                var barGroups = ["Group A", "Group B"];

                
                self.barGroupsValue = ko.observableArray(barGroups);
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new CustomerViewModel();
  }
);
