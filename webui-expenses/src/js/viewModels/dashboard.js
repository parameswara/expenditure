/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'models/all_data', 'models/exp_with', 'models/balance', 'base/windows_location', 'ojs/ojmodel', 'ojs/ojtable', 'ojs/ojcollectiontabledatasource', 'ojs/ojmodule',
        "ojs/ojpagingtabledatasource", "ojs/ojpagingcontrol", 'ojs/ojknockout', 'ojs/ojnavigationlist', 'ojs/ojswitcher', 'ojs/ojradioset',
        'ojs/ojlabel', 'ojs/ojselectcombobox',  'ojs/ojbutton', 'ojs/ojtimezonedata', 'ojs/ojdatetimepicker', 'ojs/ojmasonrylayout'],
        function (oj, ko, $, all_data, dropdown, balance, location) {

        function DashboardViewModel() {
        var self = this;
                self.incomedatasource = ko.observable();
                self.expensedatasource = ko.observable();
                self.chaindatasource = ko.observable();
                self.loansdatasource = ko.observable();
                self.returnsdatasource = ko.observable();
                self.reversaldatasource = ko.observable();
                self.selectedform = ko.observable("income");
                self.tasksVM_income = new all_data.taskVM();
                self.tasksVM_expense = new all_data.taskVM();
                self.tasksVM_chain = new all_data.taskVM();
                self.tasksVM_loans = new all_data.taskVM();
                self.tasksVM_returns = new all_data.taskVM();
                self.tasksVM_reversal = new all_data.taskVM();
                var base = new location.windowsVM();
                var baseurl = base.baseurl;
                
               
//                to determine screen size

                smQuery = oj.ResponsiveUtils.getFrameworkQuery(
                        oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                self.screenRange = oj.ResponsiveKnockoutUtils.createScreenRangeObservable();
                self.isSmall = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                
//                expense observables
                self.money_spent = ko.observable();
                self.money_source = ko.observable('sbi_cc');
                self.expense_category = ko.observable('groceries');
                self.description = ko.observable();
                self.purchase_date = ko.observable();
                self.expense_link_status= ko.observable('income');
                self.income_link = ko.observable('');
                self.chain_link = ko.observable('');
                self.loan_link = ko.observable('');
                
//                intilize all dropdown
                self.incomedropdowndata = ko.observable();
                self.chaindropdowndata = ko.observable();
                self.loansdropdowndata = ko.observable();
                self.incomedropdown = new dropdown.withexpVM();
                self.chaindropdown = new dropdown.withexpVM();
                self.loansdropdown = new dropdown.withexpVM();
                
                self.incomedropdown.exp_credits("income");
                        self.incomedropdown.fetch(function (expense_collection, response, options) {
                            self.incomedropdowndata(response.data);
                        });
          
            
//                genterate balance left
                self.incomebalance = ko.observable();
                self.incomebalancelink = new balance.balanceVM();
                
                self.chainbalance = ko.observable();
                self.chainbalancelink = new balance.balanceVM();
                
                self.loansbalance = ko.observable();
                self.loansbalancelink = new balance.balanceVM();
                
                function income_balance(){
                    self.incomebalancelink.balance_credits("income/load_income/" + self.income_link())
                    self.incomebalancelink.fetch(function (expense_collection, response, options) {
                            self.incomebalance(response.data);
                        });
                };
                
                self.income_link.subscribe(function () {
                    if (self.income_link() !== ""){
                        income_balance();
                    };    
                });
                
                function chain_balance(){
                    self.chainbalancelink.balance_credits("chain/chain/" + self.chain_link())
                    self.chainbalancelink.fetch(function (expense_collection, response, options) {
                            self.chainbalance(response.data);
                        });
                };
                
                self.chain_link.subscribe(function () {
                    if (self.chain_link() !== ""){
                        chain_balance();
                    };
                });
                
                function loans_balance(){
                    self.loansbalancelink.balance_credits("loan/loans/" + self.loan_link())
                    self.loansbalancelink.fetch(function (expense_collection, response, options) {
                            self.loansbalance(response.data);
                        });
                };
                
                self.loan_link.subscribe(function () {
                    if (self.loans_link() !== ""){
                        loans_balance();
                    };
                });
                
                
                
                
//                income observables
                self.credited_by = ko.observable();
                self.income_got = ko.observable();
                self.income_source = ko.observable('salary');
                self.income_description = ko.observable();
                self.income_date = ko.observable();
                self.income_credited_to = ko.observable('icici_dc')
                self.income_link_dropdown = ko.observableArray([]);
                
//                chain observables
                self.incomechain = ko.observable();
                self.money_spent = ko.observable();
                self.money_dest = ko.observable();
                self.chain_description = ko.observable();
                self.chain_purchase_date = ko.observable();
                
//               loan observables
                self.loancredited_by = ko.observable();
                self.loancredited_to = ko.observable('icici_dc');
                self.loan_got = ko.observable();
                self.loan_tenure = ko.observable();
                self.loandescription = ko.observable();
                self.loan_date = ko.observable();
                
//                returns observables
                self.amount = ko.observable();
                self.returndescription = ko.observable();
                self.returnpurchase_date = ko.observable();
                self.returnsreturn_link = ko.observable();
                self.returncredited_to = ko.observable();
                
//                reversal observables
                self.reversal_amount = ko.observable();
                self.reversal_source_link = ko.observable("loan");
                self.reversal_chain_link = ko.observable('');
                self.reversal_loan_link = ko.observable('');
                self.reversal_income_link = ko.observable('');
                self.reversal_returns_link = ko.observable('');
                self.reversal_description = ko.observable();
                self.reversal_purchase_date = ko.observable();
                self.reversal_reversal_loan = ko.observable('');
                
                
                
//                self.tasksVM.init_income("income");
//                self.tasksVM.fetch(function (income_collection, response, options) {
//                self.incomedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(income_collection)));
//                });
//                
//                self.tasksVM.init_expense("expense");
//                self.tasksVM.fetch(function (expense_collection, response, options) {
//                self.expensedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(expense_collection)));
//                });
                
                
                self.selectedItem = ko.observable("income");
                self.currentEdge = ko.observable("top");
                self.valueChangedHandler = function (event) {
                var value = event.detail.value,
                        previousValue = event.detail.previousValue;
                        $('#demo-container').addClass('demo-edge-' + value)
                        .removeClass('demo-edge-' + previousValue);
                }
                
                self.tasksVM_income.init_income("income");
                self.tasksVM_income.fetch(function (income_collection, response, options) {
                    income_collection.sortDirection = -1;
                    income_collection.sort()
                    self.incomedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(income_collection)));
                });
                
                self.incomedropdown.exp_credits("income");
                       self.incomedropdown.fetch(function (expense_collection, response, options) {
                           self.incomedropdowndata(response.data);
                           self.income_link(self.incomedropdowndata()[0]['id']);
                       });
         
                
                // observable bound to the Buttonset:
                self.drink = ko.observable("income");
                self.val = ko.observable("Firefox");
                
                self.selectedItem.subscribe(function () {
                    if (self.selectedItem() == 'income') {
                        self.tasksVM_income.init_income("income");
                        self.tasksVM_income.fetch(function (income_collection, response, options) {
                            income_collection.sortDirection = -1;
                            income_collection.sort();
                            self.incomedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(income_collection)));
                        });

                    } else if (self.selectedItem() == 'expense') {
                        self.tasksVM_expense.init_expense("expense");
                        self.tasksVM_expense.fetch(function (expense_collection, expense_response, expense_options) {
                            expense_collection.sortDirection = -1;
                            expense_collection.sort();
                            self.expensedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(expense_collection)));
                        });
                    } else if (self.selectedItem() == 'chain') {
                        self.tasksVM_chain.init_expense("chain");
                        self.tasksVM_chain.fetch(function (chain_collection, expense_response, expense_options) {
                            chain_collection.sortDirection = -1;
                            chain_collection.sort();
                            self.chaindatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(chain_collection)));
                        });
                    } else if (self.selectedItem() == 'loans') {
                        self.tasksVM_loans.init_expense("loans");
                        self.tasksVM_loans.fetch(function (loans_collection, expense_response, expense_options) {
                            loans_collection.sortDirection = -1;
                            loans_collection.sort();
                            self.loansdatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(loans_collection)));
                        });
                    } else if (self.selectedItem() == 'returns') {
                        self.tasksVM_returns.init_expense("returns");
                        self.tasksVM_returns.fetch(function (returns_collection, expense_response, expense_options) {
                            returns_collection.sortDirection = -1;
                            returns_collection.sort();
                            self.returnsdatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(returns_collection)));
                        });
                    } else if (self.selectedItem() == 'reversal') {
                        self.tasksVM_reversal.init_expense("reversal");
                        self.tasksVM_reversal.fetch(function (reversal_collection, expense_response, expense_options) {
                            reversal_collection.sortDirection = -1;
                            reversal_collection.sort();
                            self.reversaldatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(reversal_collection)));
                        });
                    }
                });
                
                self.expense_link_status.subscribe(function () {
                    if (self.expense_link_status() == 'income') {
                        $("#income_link").show();
                        $("#income_link_form").show();
                        $("#chain_link").hide();
                        $("#chain_link_form").hide();
                        $("#loan_link").hide();
                        $("#loan_link_form").hide();
                    } else if (self.expense_link_status() == 'chain') {
                        self.income_link('');
                        self.chaindropdown.exp_credits("chain");
                        self.chaindropdown.fetch(function (expense_collection, response, options) {
                            self.chaindropdowndata(response.data);
                            self.chain_link(self.chaindropdowndata()[0]['id']);
                            chain_balance();
                        });
                        
                        
                        $("#income_link").hide();
                        $("#income_link_form").hide();
                        $("#chain_link").show();
                        $("#chain_link_form").show();
                        $("#loan_link").hide();
                        $("#loan_link_form").hide();
                    } else if (self.expense_link_status() == 'loan') {
                        self.income_link(' ');
                        self.loansdropdown.exp_credits("loan");
                        self.loansdropdown.fetch(function (expense_collection, response, options) {
                            self.loansdropdowndata(response.data);
                        });
                        $("#income_link").hide();
                        $("#income_link_form").hide();
                        $("#chain_link").hide();
                        $("#chain_link_form").hide();
                        $("#loan_link").show();
                        $("#loan_link_form").show();
                    }
                });
                
                self.reversal_source_link.subscribe(function () {
                    if (self.reversal_source_link() == 'chain') {
                         $("#reversal_loan_link").hide();$("#reversal_loan_link_form").hide();
                        $("#reversal_chain_link").show();$("#reversal_chain_link_form").show();
                        $("#reversal_income_link").hide();$("#reversal_income_link_form").hide();
                         $("#reversal_returns_link").hide();$("#reversal_returns_link_form").hide();
                    } else if (self.reversal_source_link() == 'loan'){
                        $("#reversal_loan_link").show();$("#reversal_loan_link_form").show();
                        $("#reversal_chain_link").hide();$("#reversal_chain_link_form").hide();
                        $("#reversal_income_link").hide();$("#reversal_income_link_form").hide();
                         $("#reversal_returns_link").hide();$("#reversal_returns_link_form").hide();
                    } else if (self.reversal_source_link() == 'income'){
                        $("#reversal_loan_link").hide();$("#reversal_loan_link_form").hide();
                        $("#reversal_chain_link").hide();$("#reversal_chain_link_form").hide();
                        $("#reversal_income_link").show();$("#reversal_income_link_form").show();
                         $("#reversal_returns_link").hide();$("#reversal_returns_link_form").hide();
                    } else if (self.reversal_source_link() == 'returns'){
                         $("#reversal_loan_link").hide();$("#reversal_loan_link_form").hide();
                        $("#reversal_chain_link").hide();$("#reversal_chain_link_form").hide();
                        $("#reversal_income_link").hide();$("#reversal_income_link_form").hide();
                         $("#reversal_returns_link").show();$("#reversal_returns_link_form").show();
                    }
                });
                
                $(document).ready(function () {
                    $("#income_link").show();$("#income_link_form").show();
                    $("#chain_link").hide(); $("#chain_link_form").hide();
                    $("#loan_link").hide(); $("#loan_link_form").hide();
                });
                
                $(document).ready(function () {
                     $("#reversal_loan_link").show();$("#reversal_loan_link_form").show();
                        $("#reversal_chain_link").hide();$("#reversal_chain_link_form").hide();
                        $("#reversal_income_link").hide();$("#reversal_income_link_form").hide();
                         $("#reversal_returns_link").hide();$("#reversal_returns_link_form").hide();
                });
                
                self.drinkValues = [
                    {id: 'income', label: 'Income'},
                    {id: 'expense', label: 'Expense'},
                    {id: 'chain', label: 'Chain'},
                    {id: 'loans', label: 'Loans'},
                    {id: 'returns', label: 'Returns'},
                    {id: 'reversal', label: 'Reversal'},
                ];
                
                self.smalldrinkValues = [
                    {id: 'income', label: 'Income'},
                    {id: 'expense', label: 'Expense'},
                    {id: 'chain', label: 'Chain'},
                    {id: 'returns', label: 'Returns'},
                ];

                self.credited_to = [{value: 'sbi_cc', label: 'SBI CC'},
                    {label: 'ICICI CC', value: 'icici_cc'},
                    {label: 'HDFC CC', value: 'hdfc_cc'},
                    {label: 'ICICI DC', value: 'icici_dc'},
                    {label: 'SBI DC', value: 'sbi_dc'},
                    {label: 'HDFC DC', value: 'hdfc_dc'},
                    {label: 'KVB', value: 'kvb'},
                    {label: 'Hand Cash', value: 'hand_cash'}];

                self.income_source_options = [{value: 'salary', label: 'Salary'},
                    {value: 'project', label: 'Project'},
                    {value: 'hand_loan', label: 'Hand Loan'},
                    {value: 'gift', label: 'Gift'},
                    {value: 'loan', label: 'Loan'},
                    {value: 'cash_dep', label: 'Cash Deposited'},
                    {value: 'credit_int', label: 'Credit Interest'} ]

                self.source_link = [{value: 'chain', label: 'Chain'},
                    {value: 'loan', label: 'loan'},
                    {value: 'income', label: 'Income'},
                    {value: 'returns', label: 'Returns'}
                    ]
                    
                self.expense_cat = [{value: 'food', label: 'Food'},
                    {value: 'cloth', label: 'Cloth'},
                    {value: 'petrol', label: 'Petrol'},
                    {value: 'groceries', label: 'Groceries'},
                    {value: 'veggies', label: 'Veggies'},
                    {value: 'petrol', label: 'Petrol'},
                    {value: 'entertainment', label: 'Entertainment'},
                    {value: 'medical', label:'medical'},
                    {value: 'interest', label: 'Interest'},
                    {value: 'bank_charges', label: 'Bank Charges'},
                    {value: 'phone_charges', label: 'Phone Charges'},
                    {value: 'home_internet', label: 'Home Internet'},
                    {value: 'travel', label: 'Travel'},
                    {value: 'electric', label: 'Electrical'},
                    {value: 'cash_with', label: 'Cash Withdrawl'},
                    {value: 'loan_given', label: 'Hand Loan'},
                    {value: 'education', label: 'Education'},
                    {value: 'others', label: 'others'}

                ]
                    
                self.clickedButton = ko.observable("(None clicked yet)");
                self.expensebuttonClick = function (event) {
                    var data;
                    data = new FormData();
                    data.append('money_spent', self.money_spent());
                    data.append('money_source', self.money_source());
                    data.append('expense_category', self.expense_category());
                    data.append('description', self.description());
                    data.append('purchase_date', self.purchase_date());
                    data.append('expense_link', self.expense_link_status());
                    data.append('income_link', self.income_link());
                    data.append('chain_link', self.chain_link());
                    data.append('loan_link', self.loan_link());

                    $.ajax({
                        url: 'http://'+ baseurl + '/expense/load_expense/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert("successfully updated");
                        }
                    });

                    self.money_spent('')
                    self.money_source('sbi_cc');
                    self.expense_category('groceries');
                    self.description('');
                    self.purchase_date('');
                    if (self.expense_link_status() !== "income") {
                        self.income_link('');
                    } else {
                         income_balance();
                    };
                    self.expense_link_status('income');
                    self.chain_link('');
                    self.loan_link('');
                    return true;
                }
                
                
                self.incomebuttonClick = function (event) {
                    var data;
                    data = new FormData();
                    data.append('credited_by', self.credited_by());
                    data.append('income_got', self.income_got());
                    data.append('income_source', self.income_source());
                    data.append('description', self.income_description());
                    data.append('income_date', self.income_date());
                    data.append('credited_to', self.income_credited_to());


                    $.ajax({
                        url: 'http://'+ baseurl +'/income/load_income/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert("successfully updated");
                        }
                    });

                    self.credited_by('');
                    self.income_got('');
                    self.income_source('salary');
                    self.income_description('');
                    self.income_date('');
                    self.income_credited_to('icici_dc');

                    return true;
                }
                
                self.chainbuttonClick = function(event){
                    var data;
                    data = new FormData();
                     data.append('income_chain', self.incomechain());
                    data.append('money_spent', self.money_spent());
                    data.append('money_destination', self.money_dest());
                    data.append('description', self.chain_description());
                    data.append('purchase_date', self.chain_purchase_date());
                    
                     $.ajax({
                        url: 'http://'+ baseurl +'/chain/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert("successfully updated");
                        }
                    });
                self.incomechain('');
                self.money_spent('');
                self.money_dest('');
                self.chain_description('');
                self.chain_purchase_date('');
                    
                    return true
                };
                
                
                self.loanbuttonClick = function(event){
                    var data;
                    data = new FormData();
                     data.append('credited_by', self.loancredited_by());
                    data.append('credited_to', self.loancredited_to());
                    data.append('loan_got', self.loan_got());
                    data.append('loan_tenure', self.loan_tenure());
                    data.append('description', self.loandescription());
                    data.append('loan_date', self.loan_date());
                    
                     $.ajax({
                        url: 'http://'+ baseurl + '/loan/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert("successfully updated");
                        }
                    });
                self.loancredited_by('');
                self.loancredited_to('icici_dc');
                self.loan_got('');
                self.loan_tenure('');
                self.loandescription('');
                self.loan_date('');
                    
                    return true
                };
                
                self.returnsbuttonClick = function(event){
                    var data;
                    data = new FormData();
                     data.append('amount', self.amount());
                    data.append('description', self.returndescription());
                    data.append('purchase_date', self.returnpurchase_date());
                    data.append('return_link', self.returnsreturn_link());
                    data.append('credited_to', self.returncredited_to());
                    
                     $.ajax({
                        url: 'http://'+ baseurl +'/returns/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert("successfully updated");
                        }
                    });
                self.amount('');
                self.returndescription('');
                self.returnpurchase_date('');
                self.returnsreturn_link('');
                self.returncredited_to('');
                    
                    return true
                };
                
                self.reversalbuttonClick = function(event){
                    var data;
                    data = new FormData();
                     data.append('reversal_amount', self.reversal_amount());
                    data.append('source_link', self.reversal_source_link());
                    data.append('chain_link', self.reversal_chain_link());
                    data.append('loan_link', self.reversal_loan_link());
                    data.append('income_link', self.reversal_income_link());
                    data.append('returns_link', self.reversal_returns_link());
                    data.append('description', self.reversal_description());
                    data.append('purchase_date', self.reversal_purchase_date());
                    data.append('reversal_loan', self.reversal_reversal_loan());
                    
                     $.ajax({
                        url: 'http://'+ baseurl +'/reversal/',
                        data: data,
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        type: 'POST',
                        success: function (data) {
                            alert("successfully updated");
                        }
                    });
                self.reversal_amount('');
                self.reversal_source_link("loan");
                self.reversal_chain_link('');
                self.reversal_loan_link('');
                self.reversal_income_link('');
                self.reversal_returns_link('');
                self.reversal_description('');
                self.reversal_purchase_date('');
                self.reversal_reversal_loan('');
                    
                    return true
                };

                self.buttonClick = function (event) {
                    self.clickedButton(event.currentTarget.id);
                    return true;
                }
                // Below are a subset of the ViewModel methods invoked by the ojModule binding
                // Please reference the ojModule jsDoc for additional available methods.

                /**
                 * Optional ViewModel method invoked when this ViewModel is about to be
                 * used for the View transition.  The application can put data fetch logic
                 * here that can return a Promise which will delay the handleAttached function
                 * call below until the Promise is resolved.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
                 * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new DashboardViewModel();
  }
);
