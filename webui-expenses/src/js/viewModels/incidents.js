/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'models/all_data', 'ojs/ojarraydataprovider', 'ojs/ojmodel', 'ojs/ojtable', 'ojs/ojcollectiontabledatasource', 'ojs/ojmodule',
    "ojs/ojpagingtabledatasource", "ojs/ojpagingcontrol",  'ojs/ojswitcher',  'ojs/ojdatacollection-utils'],
        function (oj, ko, $, all_data) {

            function IncidentsViewModel() {
                var self = this;

                self.selectedItem = ko.observable("income");
                self.incomedatasource = ko.observable();
                self.incomeObservableArray = ko.observableArray();
                self.incomeprovider = ko.observable();
                self.expensedatasource = ko.observable();
                self.chaindatasource = ko.observable();
                self.loansdatasource = ko.observable();
                self.returnsdatasource = ko.observable();
                self.reversaldatasource = ko.observable();
                self.tasksVM_income = new all_data.taskVM();
                self.tasksVM_expense = new all_data.taskVM();
                self.tasksVM_chain = new all_data.taskVM();
                self.tasksVM_loans = new all_data.taskVM();
                self.tasksVM_returns = new all_data.taskVM();
                self.tasksVM_reversal = new all_data.taskVM();
                self.currentEdge = ko.observable("top");
      
                
                 self.tasksVM_income.init_income("income");
                self.tasksVM_income.fetch(function (income_collection, response, options) {
//                    self.incomedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(income_collection)));
//                     self.incomeObservableArray.push(response.data);
                     self.incomeprovider(new oj.ArrayDataProvider(response.data, {keyAttributes: 'id'}));
                });
                
                self.selectedItem.subscribe(function () {
                     if (self.selectedItem() == 'expense') {
                        self.tasksVM_expense.init_expense("expense");
                        self.tasksVM_expense.fetch(function (expense_collection, expense_response, expense_options) {
                            self.expensedatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(expense_collection)));
                        });
                    } else if (self.selectedItem() == 'chain') {
                        self.tasksVM_chain.init_expense("chain");
                        self.tasksVM_chain.fetch(function (chain_collection, expense_response, expense_options) {
                            self.chaindatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(chain_collection)));
                        });
                    } else if (self.selectedItem() == 'loans') {
                        self.tasksVM_loans.init_expense("loans");
                        self.tasksVM_loans.fetch(function (loans_collection, expense_response, expense_options) {
                            self.loansdatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(loans_collection)));
                        });
                    } else if (self.selectedItem() == 'returns') {
                        self.tasksVM_returns.init_expense("returns");
                        self.tasksVM_returns.fetch(function (returns_collection, expense_response, expense_options) {
                            self.returnsdatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(returns_collection)));
                        });
                    } else if (self.selectedItem() == 'reversal') {
                        self.tasksVM_reversal.init_expense("reversal");
                        self.tasksVM_reversal.fetch(function (reversal_collection, expense_response, expense_options) {
                            self.reversaldatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(reversal_collection)));
                        });
                    }
                });
                // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new IncidentsViewModel();
  }
);
